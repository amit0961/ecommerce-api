<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::Resource('/products','ProductController'); //for all  routes , do check it out by this command -> php artisan route:list
Route::apiResource('/products','ProductController'); //for only api routes

Route::group(['prefix'=>'products'], function(){
    Route::apiResource('/{product}/reviews/' ,'ReviewController');
});
