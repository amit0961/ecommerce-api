<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\Resource;

class ProductCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=>$this->name ,
            'discountAmount'=>(($this->price* $this->discount)/100),
            'totalAmount'=> ($this->price - (($this->price* $this->discount)/100) ),
            'rating'=> $this->reviews->count() > 0 ? round($this->reviews->sum('star')/$this->reviews->count('star')) : 'No Rating Yet',
            'href'=> [
                'link' => route('products.show', $this->id)
            ]
        ];
    }
}
